// Copyright 2016-2017 John J Foerch. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//    2. Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the
//       distribution.
//
// THIS SOFTWARE IS PROVIDED BY JOHN J FOERCH ''AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL JOHN J FOERCH OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/*
 * Note Handlers
 */

var note_handlers = {};
var control_change_handlers = {};
var pitch_bend_handlers = {};

/*
 * Midi Handlers
 */

function note_on (event, channel) {
    var note = event[1];
    var velocity = event[2] / 127;
    var note_handler = note_handlers.all || note_handlers[channel];
    if (note_handler) {
        note_handler(channel, note, velocity);
    }
}

function note_off (event, channel) {
    event[2] = 0;
    return note_on(event, channel);
}

function control_change (event, channel) {
    var controller = event[1];
    var value = event[2] / 127;
    var control_change_handler = control_change_handlers.all ||
        control_change_handlers[channel];
    if (control_change_handler) {
        control_change_handler(channel, controller, value);
    }
}

function pitch_bend (event, channel) {
    var value = ((event[1] + event[2] * 128) - 0x2000) / 0x2000;
    var pitch_bend_handler = pitch_bend_handlers.all ||
        pitch_bend_handlers[channel];
    if (pitch_bend_handler) {
        pitch_bend_handler(channel, value);
    }
}

function dispatch (event) {
    var status = event[0];
    var messagetype = status & 0xf0;
    var channel = status & 0x0f;

    var handlers = {
        0x80: note_off,
        0x90: note_on,
        // 0xa0: aftertouch,
        0xb0: control_change, // also channel mode (for controllers 120--127)
        // 0xc0: program_change,
        // 0xd0: channel_pressure,
        0xe0: pitch_bend,
        // 0xf0: system_exclusive
        
    };
    var handler = handlers[messagetype];
    if (handler) {
        handler(event, channel);
    } else {
        //print(messagetype + " channel: " + channel);
    }
}


/*
 * Device Selectors
 */

function any_device () {
    return true;
}


/*
 * Main
 */

function start (preferred_device) {
    var names = MIDI.getDeviceNames();
    var device = null;
    if (typeof preferred_device == "function") {
        for (var i = 0, n = names.length; i < n; ++i) {
            if (preferred_device(names[i])) {
                device = names[i];
            }
        }
    } else if (typeof preferred_device == "string") {
        if (names.indexOf(preferred_device) > -1) {
            device = preferred_device;
        }
    }
    if (device == null) {
        throw Error("No suitable device found.");
    }
    print("Using device "+device);
    var port = new MIDI(device);
    port.openRead();
    while (true) {
        var n = port.waitForData();
        for (var i = 0; i < n; ++i) {
            var event = port.readArray();
            dispatch(event);
        }
    }
    port.close();
}
