// Copyright 2016-2017 John J Foerch. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//    2. Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the
//       distribution.
//
// THIS SOFTWARE IS PROVIDED BY JOHN J FOERCH ''AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL JOHN J FOERCH OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

include("./midi.js");


/*
 * Utils
 */

var colors = {
    black: { r: 0, g: 0, b: 0 },
    white: { r: 1, g: 1, b: 1 },
    red: { r: 1, g: 0, b: 0 },
    green: { r: 0, g: 1, b: 0 },
    blue: { r: 0, g: 0, b: 1 },
    yellow: { r: 1, g: 1, b: 0 },
    cyan: { r: 0, g: 1, b: 1 },
    magenta: { r: 1, g: 0, b: 1 }
};

function array_contains (ar, el) {
    for (var i = 0, n = ar.length; i < n; ++i) {
        if (ar[i] == el)
            return true;
    }
    return false;
}

function hsl_to_rgb (h, s, l) {
    if (s == 0) {
        return { r: 1, g: 1, b: 1}; // achromatic
    } else {
        var hue2rgb = function (p, q, t) {
            if (t < 0) { t += 1; }
            if (t > 1) { t -= 1; }
            if (t < 1/6) { return p + (q - p) * 6 * t; }
            if (t < 1/2) { return q; }
            if (t < 2/3) { return p + (q - p) * (2/3 - t) * 6; }
            return p;
        };
        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;
        return { r: hue2rgb(p, q, h + 1/3),
                 g: hue2rgb(p, q, h),
                 b: hue2rgb(p, q, h - 1/3) }
    }
}


/*
 * Chromacove
 */

var GLOBAL_BRIGHTNESS = 1.0;
function brightness (x) {
    GLOBAL_BRIGHTNESS = x;
}

var opened_sockets = [];
function chromacove_rgb_writer (socket) {
    if (! array_contains(opened_sockets, socket)) {
        socket.openClientUDP();
        opened_sockets.push(socket);
    }
    return function (color, duration) {
        var r = color.r * GLOBAL_BRIGHTNESS;
        var g = color.g * GLOBAL_BRIGHTNESS;
        var b = color.b * GLOBAL_BRIGHTNESS;
        socket.write("SET_RGB "+r+","+g+","+b+","+(duration || 0));
    };
}

function chromacove_cue_writer (socket) {
    if (! array_contains(opened_sockets, socket)) {
        socket.openClientUDP();
        opened_sockets.push(socket);
    }
    return function (cue) {
        socket.write("RUN_CUE "+cue);
    };
}


/*
 * MIDI
 */

var nnotes_on = {};

function update_nnotes_on (channel, note, velocity) {
    var c = nnotes_on[channel] || 0;
    if (velocity == 0) {
        c = Math.max(c - 1, 0);
    } else {
        c++;
    }
    nnotes_on[channel] = c;
    return c;
}


/*
 * Color Generators
 */

function note_to_white (channel, note, velocity) {
    var duration = 0.2;
    if (nnotes_on[channel] > 0) {
        return [colors.white, duration];
    } else {
        return [colors.black, duration];
    }
}

function note_to_random_color (channel, note, velocity) {
    var duration = 0.2;
    if (nnotes_on[channel] > 0) {
        return [{ r: Math.random(), g: Math.random(), b: Math.random()}, duration];
    } else {
        return [colors.black, duration];
    }
}

function note_to_hue (channel, note, velocity) {
    var duration = 0.2;
    if (nnotes_on[channel] > 0) {
        return [hsl_to_rgb(note / 128, 1, 0.5), duration];
    } else {
        return [colors.black, duration];
    }
}

function note_to_assigned_color$ (assignments) {
    return function (channel, note, velocity) {
        var duration = 0.2;
        if (nnotes_on[channel] > 0) {
            var color = assignments[note];
            if (typeof color == "string") {
                color = colors[color];
            } else if (! color) {
                color = hsl_to_rgb(Math.random(), 1, 0.2);
                assignments[note] = color;
            }
            return [color, duration];
        } else {
            return [colors.black, duration];
        }
    };
}


/*
 * Routers
 */

function route_notes (channel, processor, writer) {
    writer = writer || function () {};
    nnotes_on[channel] = 0;
    note_handlers[channel] = function (channel, note, velocity) {
        update_nnotes_on(channel, note, velocity);
        var data = processor(channel, note, velocity);
        if (data) {
            writer.apply(this, data);
        }
    };
}

function route_note_on_trigger (channel, processor, writer) {
    writer = writer || function () {};
    nnotes_on[channel] = 0;
    note_handlers[channel] = function (channel, note, velocity) {
        if (velocity > 0) {
            var data = processor(channel, note, velocity);
            if (data) {
                writer.apply(this, data);
            }
            delete note_handlers[channel];
            delete nnotes_on[channel];
        }
    };
}

/**
 * Processor receives note_on until and including a note_off.
 */
function route_note_off_trigger (channel, processor, writer) {
    writer = writer || function () {};
    nnotes_on[channel] = 0;
    var flag = false;
    note_handlers[channel] = function (channel, note, velocity) {
        if (update_nnotes_on(channel, note, velocity)) {
            flag = true;
        } else if (flag) {
            delete note_handlers[channel];
            delete nnotes_on[channel];
        }
        var data = processor(channel, note, velocity);
        if (data) {
            writer.apply(this, data);
        }
    };
}

/**
 * route_reader can be attached to a channel with route_notes, then when
 * it recieves an event, it checks a given Digistar object.attribute for a
 * command - if it finds a new command, it evals it.  That command can
 * install new routes or do anything else.  Route_reader protects the
 * channel it is itself installed on, and if another route overrides it,
 * that route becomes a child route.
 */
function route_reader (dsattr) {
    var sp = dsattr.split(".");
    var ref = Ds.NewObjectAttrRef(sp[0], sp[1]);
    Ds.SetObjectAttrUsingRef(ref, "");
    var prev_val = "";
    var child = null;
    var self = function (channel, note, velocity) {
        try {
            // read and evaluate a route from dsob,attr
            var val = Ds.GetObjectAttrUsingRef(ref);
            if (val != prev_val) {
                prev_val = val;
                eval(val);
                // reinstall self if we were overwritten
                if (note_handlers[channel] != self) {
                    child = note_handlers[channel];
                    note_handlers[channel] = self;
                }
            }
        } catch (e) {
            note_handlers[channel] = self;
            Ds.SendMessageError(e.message);
        }
        if (child) {
            child(channel, note, velocity);
        }
    };
    return self;
}
